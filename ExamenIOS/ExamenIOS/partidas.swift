//
//  partidas.swift
//  ExamenIOS
//
//  Created by Jose Diaz on 5/12/17.
//  Copyright © 2017 JoDiaz. All rights reserved.
//

import Foundation
import ObjectMapper

class Partidas: Mappable {
    var spectators:Int?
    var mmr:Int?
    var players:[Players]?
    var lobbyid:String?
    var direScore:Int?
    var radiantScore:Int?
    
    required init? (map: Map){
        
    }
    
    func mapping (map: Map){
        spectators <- map["spectators"]
        mmr <- map["average_mmr"]
        players <- map["players"]
        lobbyid <- map["lobby_id"]
        radiantScore <- map["radiant_score"]
        direScore <- map["dire_score"]
        
    }
}
class Players: Mappable {
    var idaccount:Int?
    
    required init?(map: Map) {

    }
    func mapping(map: Map) {
        idaccount <- map["hero_id"]
    }
}

class JugadoresPro: Mappable {
    var name:String?
    var teamname:String?
    
    required init?(map: Map) {
    }
    
    func mapping(map: Map) {
        name <- map["name"]
        teamname <- map["team_name"]
    }
}
