//
//  ViewController.swift
//  ExamenIOS
//
//  Created by Jose Diaz on 5/12/17.
//  Copyright © 2017 JoDiaz. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var matchesArray = Array<Partidas>()
    override func viewDidLoad() {
        super.viewDidLoad()
        /*let URL = "https://api.opendota.com/api/live"
        //Tomar en consideracion que el DataResponse<Pokemon> es de la clase pokemon creada en el archivo pokemon.switft
        Alamofire.request(URL).responseArray { (response: DataResponse<[Partidas]>) in
            let matchesArray = response.result.value
            
            if let matchesArray = matchesArray {
                var cont = 0
                for matchinfo in matchesArray {
                    if (cont < 5){
                        print(matchinfo.spectators)
                        print(matchinfo.mmr)
                        
                        for players in (matchinfo.players)!{
                            print(players.idaccount)
                        }
                        
                        cont+=1
                    }
                }
            }
        }*/
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBOutlet weak var tableView: UITableView!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func consultarButtonPressed(_ sender: Any) {
        /*let URL = "https://api.opendota.com/api/live"
        //Tomar en consideracion que el DataResponse<Pokemon> es de la clase pokemon creada en el archivo pokemon.switft
        Alamofire.request(URL).responseArray { (response: DataResponse<[Partidas]>) in
            let matchesArray = response.result.value
            
            if let matchesArray = matchesArray {
                var cont = 0
                for matchinfo in matchesArray {
                    if (cont < 5){
                        print(matchinfo.spectators)
                        print(matchinfo.mmr)
                        
                        for players in (matchinfo.players)!{
                            print(players.idaccount)
                        }
                        
                        cont+=1
                    }
                }
            }
        }*/
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let URL = "https://api.opendota.com/api/live"
        //Tomar en consideracion que el DataResponse<Pokemon> es de la clase pokemon creada en el archivo pokemon.switft
        Alamofire.request(URL).responseArray { (response: DataResponse<[Partidas]>) in
            self.matchesArray = response.result.value!
            
            /*if let matchesArray = matchesArray {
                var cont = 0
                for matchinfo in matchesArray {
                    if (cont < 5){
                        
                        print(matchinfo.spectators)
                        print(matchinfo.mmr)
                        
                        for players in (matchinfo.players)!{
                            print(players.idaccount)
                        }
                        
                        cont+=1
                    }
                }
            }*/
            
            for section in 0..<tableView.numberOfSections {
                for row in 0..<tableView.numberOfRows(inSection: section) {
                    
                    let indexPath = NSIndexPath(row: row, section: section)
                    var cell = tableView.cellForRow(at: indexPath as IndexPath)
                    
                    cell?.textLabel?.text = "Match ID: \(self.matchesArray[row].lobbyid!)"
                    // do what you want with the cell
                    
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title = "Partidas Jugadas Actualmente"
        return title
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let text = self.matchesArray[indexPath.row]
        self.performSegue(withIdentifier: "miSegue", sender: text)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?){
        if segue.identifier == "miSegue", let destination = segue.destination as? SecondViewController {
            if let cell = sender as? Partidas {
                destination.matchid = cell.lobbyid!
                destination.radiantScore = cell.radiantScore
                destination.direScore = cell.direScore
            }
        }
    }
    
}

