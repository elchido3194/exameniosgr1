//
//  ListaJugadoresViewController.swift
//  ExamenIOS
//
//  Created by Jose Diaz on 13/12/17.
//  Copyright © 2017 JoDiaz. All rights reserved.
//

import UIKit
import Alamofire
import AlamofireObjectMapper

class ListaJugadoresViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    var jugadoresArray = Array<JugadoresPro>()
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 100
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell()
        let URL = "https://api.opendota.com/api/proPlayers"
        //Tomar en consideracion que el DataResponse<Pokemon> es de la clase pokemon creada en el archivo pokemon.switft
        Alamofire.request(URL).responseArray { (response: DataResponse<[JugadoresPro]>) in
            self.jugadoresArray = response.result.value!
            
            /*if let matchesArray = matchesArray {
             var cont = 0
             for matchinfo in matchesArray {
             if (cont < 5){
             
             print(matchinfo.spectators)
             print(matchinfo.mmr)
             
             for players in (matchinfo.players)!{
             print(players.idaccount)
             }
             
             cont+=1
             }
             }
             }*/
            
            for section in 0..<tableView.numberOfSections {
                for row in 0..<tableView.numberOfRows(inSection: section) {
                    
                    let indexPath = NSIndexPath(row: row, section: section)
                    var cell = tableView.cellForRow(at: indexPath as IndexPath)
                    
                    cell?.textLabel?.text = "Nombre: \(self.jugadoresArray[row].name!) Team: \(self.jugadoresArray[row].teamname ?? "No Team")"
                    // do what you want with the cell
                    
                }
            }
        }
        return cell
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        let title = "Jugadores Profesionales"
        return title
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
