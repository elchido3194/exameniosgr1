//
//  SecondViewController.swift
//  ExamenIOS
//
//  Created by Jose Diaz on 12/12/17.
//  Copyright © 2017 JoDiaz. All rights reserved.
//

import UIKit

class SecondViewController: UIViewController {

    var matchid = ""
    var radiantScore:Int?
    var direScore:Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //self.labelMatch.text = nombre
        self.radiantScoreLabel.text = "\(radiantScore!)"
        self.direScoreLabel.text = "\(direScore!)"
        self.matchIdLabel.text = matchid

        // Do any additional setup after loading the view.
    }
    @IBOutlet weak var labelMatch: UILabel!
    @IBOutlet weak var radiantScoreLabel: UILabel!
    @IBOutlet weak var direScoreLabel: UILabel!
    @IBOutlet weak var matchIdLabel: UILabel!
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func destino(dest:Partidas){
        
        print(dest.lobbyid)
        self.labelMatch.text = dest.lobbyid
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
